<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class VerifyPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $userid = Auth::user()->getAuthIdentifier();
        $user = User::find($userid);
        if(!$user->hasRole('translation-manage-translations')){
            return redirect('/');
        }

        return $next($request);
    }
}
