<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index(Request $request)
    {

        if ($request->input('role-option')) {
            $selected_role = Role::find($request->input('role-option'));
        }

        $roles = Role::all();
        $permissions = Permission::all();

        $permission_types = array();
        foreach ($permissions as $permission) {
            $permission_type = explode('-', $permission->name)[0];
            array_push($permission_types, $permission_type);
        }

        $permission_types = array_unique($permission_types);


        return view('roles.index',
            ['permission_types' => $permission_types,
                'permissions' => $permissions,
                'roles' => $roles,
                'selected_role' => $selected_role ?? null
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('roles.create');
    }


    public function storeRole(Request $request)
    {
        //

        $role_name = str_replace(" ", "-", strtolower($request->input('role-name')));
        $role = Role::create(['name'=> $role_name]);

        $request->session()->flash('status-green', 'New role "' . $role_name . '"" has been created. Please assign permissions to this role from the list below.');
        return redirect(route('roles.index', ['role-option' => $role]));

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    $permissions = Permission::all();
    $roles = Role::all();

        $selected_role = Role::where('id',$request->input('selected-role'))->first();
        $permissions_id = $request->input('permissions');
        print_r($permissions_id);
        $assigned_permissions = Permission::whereIn('id', $permissions_id)->get();
        $selected_role->syncPermissions($assigned_permissions);

        $request->session()->flash('status-green', 'The permissions given to role "' . $selected_role->name . '" has been updated.' );
        return back();
    }


    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {

        $role = Role::where('id', $id)->first();

        $users = User::role($role)->get();
        if($users){
            foreach($users as $user){
                $user->removeRole($role->name);
            }
        }

        $permissions = Permission::all();
        if($permissions){
            foreach($permissions as $permission){
                if($role->hasPermissionTo($permission->name)){
                    $role->revokePermissionTo($permission->name);
                }
            }
        }

        $role->delete();


        $request->session()->flash('status-red', 'The role "' . $role->name . '"" has been deleted. Users are no longer possessing this role.');
        return redirect(route('roles.index'));


    }
}
