<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

//use Spatie\Permission\Contracts\Role;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function assignRoleToUser($role, $user){

        $user = User ::where('name',$user)->first();


        if($user->hasRole($role)){
            return $user->name . " possessed a role of " . $role . " already.";
        }
        else{
            $user->assignRole($role);
            return "Role " . $role . " is assigned to " . $user->name;
        }

    }

    public function createPermission($type, $value)
    {

        if($type == "role"){

            $role = Role::create(['name' => $value]);
            return $role;

//            $role = Role::where('name', $value)->get();
//            if(count($role) != 0){
//                return "Role " . $value . " has been created";
//            }
//
//
//        }elseif($type == "permission") {
//
//            Permission::create(['name' => $value]);
//
//            $permission = Permission::where('name', $value)->get();
//            if(count($permission) != 0){
//                return "Permission " . $value . " has been created";
//            }
        }

    }

    public function assignPermissionToRole($role, $permission)
    {

     $r = Role::where('name', $role)->first();
     $p = Permission::where('name', $permission)->first();

       $r->givePermissionTo($p);
//       $r->assignRole($r);
       if($r->hasPermissionTo($p->name)){
           return "The role " . $r->name . " granted permission to " . $p->name;
       }
//

    }


    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
