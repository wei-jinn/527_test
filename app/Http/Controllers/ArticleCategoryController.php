<?php

namespace App\Http\Controllers;

use App\ArticleCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Stichoza\GoogleTranslate\GoogleTranslate;


class ArticleCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $categories = ArticleCategory::all();

//        $translate = true;
//        if($translate){
            $tr = new GoogleTranslate('en');
//            foreach($categories as $key => $value){
//                $categories[$key]->title = GoogleTranslate::trans($categories[$key]->title, app()->getLocale());
//            }
//        }


        return view('articles.categories.index', compact('categories', 'tr'));

    }

    public function listSubcategories($id)
    {
        //

        if($id == 0){
            $parents = ArticleCategory::where('upper_level', $id)->get();
        }else{
            $parents = ArticleCategory::where('id', $id)->get();
        }

        return view('articles.categories.category-treeview', compact('parents'));

    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = ArticleCategory::all();
        //
        return view('articles.categories.create', compact('categories'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $category = new ArticleCategory();
        $category->title = $request->input('title');
        $category->description = $request->input('description');
        $category->slug = $request->input('slug');

        $category->upper_level = $request->input('parent-category')?$request->input('parent-category'):0;
        $category->save();

        $request->session()->flash('status-green', 'New category "' . $category->title . '" has been created.' );
        return redirect(route('article-category.index'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $category = ArticleCategory::find($id);


            foreach($category->subcategory as $subcategory)
            {
               print_r($subcategory->id);
                echo "<br>";

                if(count($subcategory->subcategory)){
                         $this->show($subcategory->id);



                }


        }


    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $current_category = ArticleCategory::find($id);
        $categories = ArticleCategory::all();


        return view('articles.categories.edit', compact('categories', 'current_category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
                $category = ArticleCategory::find($id);
                $category->title = $request->input('title');
                $category->description = $request->input('description');
                $category->slug = $request->input('slug');
                $category->upper_level = $request->input('parent-category');
                $category->save();

        $request->session()->flash('status-green', '"' . $category->title . '" has been updated.' );
        return redirect()->route('article-category.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        //
        $category = ArticleCategory::find($id);
        $category->delete();
        $request->session()->flash('status-red', '"' . $category->title . '" has been removed.' );
        return redirect(route('article-category.index'));
    }
}
