<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $users = User::all();

        return view('users.index',
            [
                'users' => $users,

            ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        $roles = Role::all();

        return view('users.create',
        ['roles' => $roles]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

//        return $request->all();

        $validated = $request->validate([

            'name' => 'required',
            'email'  => 'email|required|string|unique:users',
            'password' => 'required|confirmed|min:8',
            'phone' => 'nullable|numeric',
            'address' => 'nullable|string'
            ]);


            $user = new User();
            $user->name = $validated['name'];
            $user->email = $validated['email'];
            $user->email_verified_at = now();
            $user->phone_number = $validated['phone'];
            $user->address = $validated['address'];
            $user->birthdate = $request->get('birthdate');
            $user->password = Hash::make($validated['password']);
            $user->created_at = now();
            $user->updated_at = now();
            $user->save();

        if($request->input('role')){

            $role = Role::where('id' , $request->input('role'))->first();
            $user->assignRole($role->name);
        }


        $request->session()->flash('status-green', 'New user "' . $user->name . '"" has been created.');
        return redirect(route('users.index'));


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $user = User::find($id);
        $roles = Role::all();
        $user_role = null;
//
//        return $id;

        foreach($roles as $role){
            if($user->hasRole($role)){
                $user_role = $role;
            }
        }

        return view('users.edit',
            ['user' => $user,
             'roles' => $roles,
             'user_role' => $user_role]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


        $user = User::find($id);
//        $selected_role = Role::where('id' , $request->input('role'))->get();

        $validated = $request->validate([
            'name' => 'required|string',

            'phone' => 'numeric|nullable',
            'address' => 'string|nullable'

        ]);

        $user->name = $validated['name'];
        $user->email = $request->input('email');
        $user->email_verified_at = now();
        $user->phone_number = $validated['phone'];
        $user->address = $validated['address'];
        $user->birthdate = $request->get('birthdate');

        if($request->input('old-password') || $request->input('password') || $request->input('password_confirmation')) {
        $validated = $request->validate([

            'password' => 'required|confirmed|min:8',
            'old-password' => 'required',

        ]);

            if (Hash::check($validated['old-password'], $user->password)) {
                $user->password = Hash::make($validated['password']);
            }else{
                $request->session()->flash('status-red', ' Old password is incorrect. Please try again.');
                return redirect(route('users.edit', [$user->id]));
            }
        }

        if($request->input('role')== null || $request->input('role') =='no-role'){
            $roles = Role::all();
            foreach($roles as $role){
                if($user->hasRole($role->name)){
                    $user->removeRole($role->name);
                }
            }
        }else{
            $role = Role::where('id', $request->input('role'))->first();
            $user->syncRoles($role->name);
        }





        $user->updated_at = now();
        $user->save();

        $request->session()->flash('status-green', $user->name . ' \'s profile updated.');
        return redirect(route('users.index'));


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        //
        $user = User::find($id);
        $user->delete();
        $request->session()->flash('status-red', $user->name.' \'s profile has been deleted.');
        return back();

    }

    public function uploadPhoto(Request $request)
    {

        $user = Auth::user();
        return view('users.upload',['user' =>$user]);
    }

    public function upload(Request $request){

        $user = Auth::user();
        $roles = Role::all();
        $user_role = null;

        foreach($roles as $role){
            if($user->hasRole($role)){
                $user_role = $role;
            }
        }

        if($file = $request->file('photo')){
//            $name = $request->file('photo')->getClientOriginalName();
//           $file->move('images', $name);

//            return $file;
            //$filename = $file->getClientOriginalName(); //檔案原名稱
            $extension = $file->getClientOriginalExtension(); //副檔名
            $filename = time() . "." . $extension;    //重新命名
            $file->move(storage_path('app/public/uploads/user/profile/'), $filename); //移動至指定目錄
//            $file->move(('storage'), $filename); //移動至指定目錄
            $user->photo =  $filename;
            $user->save();

            return view('users.edit',
            [
                'user' => $user,
                'roles' => $roles,
                'user_role' => $user_role
            ]);

        }
        else{
            return "No photo uploaded.";
        }


    }
}
