<?php

namespace App\Http\Controllers;

use App\Participant;
use App\User;
use Illuminate\Http\Request;

class ParticipantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

       $participants = Participant::all();

       return view('participants.index', ['participants' => $participants]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('participants.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $participant = new Participant();
        $participant->name = $request->input('name');
        $participant->prize = 0;
        $participant->save();
        $request->session()->flash('status-green', 'New participant "' . $request->input('name') . '"" has been created.');
        return redirect(route('participants.create'));


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    public function luckydraw(Request $request)
    {
        //
//        $request->session()->flash('status-green', 'Hello!');

        return view('participants.luckydraw');
    }

    public function draw(Request $request)
    {
        //
        sleep(4);
        $participants = Participant::where('prize', '0')->get();
//        return count($participants);
        $number = rand(0, count($participants));
        $participants[$number]->prize = -1;
        $participants[$number]->save();
        $request->session()->flash('status-green', 'The winner is ' .  $participants[$number]->name . '. Congratulations! .');
//            return $participants[$number]->prize;
//            return $number;
        return redirect(route('participants.luckydraw'));


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request , $id)
    {
        //
        $participant = Participant::find($id);
        $participant->delete();
        $request->session()->flash('status-red', 'The winner is ' .  $participant->name . ' has been deleted .');
        return redirect(route('participants.index'));

    }
}
