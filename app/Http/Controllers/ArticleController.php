<?php

namespace App\Http\Controllers;

use App\Article;
use App\ArticleCategory;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Stichoza\GoogleTranslate\GoogleTranslate;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
* //     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *      * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */


    public function translate(Request $request){

        if(Session::has('locale')){
            $locale = Session::get('locale');
        }

      $phrase = "Hello";
      $outcome = GoogleTranslate::trans($phrase,'cn');

      $result =  "The source: ". $phrase . "<br>";
      $result .= "<div class='btn btn-danger'>The translated outocome in " . $locale . " : " . $outcome . "</div>";

        return view('articles.translation', ['result' => $result]);

    }

    public function filter(Request $request)
    {
//

        $articles = Article::where('id','!=','0')->get();
        $selected_category = ArticleCategory::find( $request->get('filter-category'));

        if($request->get('filter-category') == 0 && $request->get('filter-author') == 0 && $request->get('filter-password') == '') {
            $articles = Article::where('id','!=','0')->get();
        }

        if($request->get('filter-keywords')!= '') {
            $keywords = $request->get('filter-keywords');
            $articles = Article::where('title', 'LIKE','%'.$keywords .'%')->get();
            $selected_keywords = $request->get('filter-keywords');

        }
        if($request->get('filter-category') !=0){
            $articles = $articles->where('category_id', '=', $request->get('filter-category'));
            $selected_category = ArticleCategory::find( $request->get('filter-category'));

        }
        if($request->get('filter-author') != 0){
            $articles = $articles->where('author_id', '=' , $request->get('filter-author'));
                $selected_author = User::find($request->get('filter-author'));
        }
         if($request->get('filter-start-date')){
                $articles = $articles->where('created_at', '>=', $request->get('filter-start-date').' 00:00:00');
                $selected_start_date = $request->get('filter-start-date');

        }
        if($request->get('filter-end-date')){
            $articles = $articles->where('created_at', '<=' , $request->get('filter-end-date'). '23:59:59');
            $selected_end_date = $request->get('filter-end-date');
        }



        $author_collection = array();

        $category_collection = array();
        $articles_temp = Article::all();
        foreach($articles_temp as $article){
            if(!in_array($article->author->id, $author_collection)){
                array_push($author_collection, $article->author->id);
            }
            if(!in_array($article->category->id, $category_collection)){
                array_push($category_collection, $article->category->id);
            }
        }

        $authors = DB::table('users')
            ->whereIn('id', $author_collection)
            ->get();

        $categories = DB::table('article_categories')
            ->whereIn('id', $category_collection)
            ->get();

        return view('articles.index',
            ['articles'=> $articles,
                'categories' => $categories,
                'authors' => $authors,
                'selected_author' => $selected_author?? null,
                'selected_category' => $selected_category?? null,
                'selected_keywords' =>$selected_keywords??null,
                'selected_start_date' => $selected_start_date??null,
                'selected_end_date' => $selected_end_date??null
            ]);


//        $articles = Article::where('created_at', '>=' ,$start_date)->get();
//
    }


    public function index()
    {
        $articles = Article::all();
        $author_collection = array();
        $category_collection = array();
        $locale = ['language' => app()->getLocale()];

        foreach($articles as $article){

            if($article->author){
                if( !in_array($article->author->id, $author_collection)){
                    array_push($author_collection, $article->author->id);
            }
            }
            if($article->category){
                if(!$article->category && !in_array($article->category->id, $category_collection)){
                    array_push($category_collection, $article->category->id);
                }
            }

        }

        $authors = User::all()
                    ->whereIn('id', $author_collection);


        $categories = ArticleCategory::all()
            ->whereIn('id', $category_collection);


        return view('articles.index', ['articles' =>$articles, 'categories' => $categories, 'authors' => $authors,
         'selected_author' => $selected_author?? null,
                'selected_category' => $selected_category?? null,
            'selected_keywords' =>$selected_keywords??null,
            'selected_start_date' => $selected_start_date??null,
            'selected_end_date' => $selected_end_date??null
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        $categories = ArticleCategory::all();

        return view('articles.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        // Validate posted form data
        $validated = $request->validate([
            'title' => 'required|string',
            'content' => 'required|string',
            'category' => 'required'
        ]);

        // Create slug from title
        $validated['slug'] = Str::slug($validated['title'], '-');
        $user = Auth::user();

        // Create and save post with validated data
//        $article = Article::create($validated);
        $article = new Article;
        $article->title = $validated['title'];
        $article->content = $validated['content'];
        $article->category_id = $validated['category'];
        $article->status = 1;
        $article->slug = Str::slug($validated['title'], '-');
        $article->author_id = $user->getAuthIdentifier();
        $article->save();

        // Redirect the user to the created post with a success notification
        $request->session()->flash('status-green', 'New article "' . $article->title . '" has been created.' );
        return redirect(route('articles.show', [$article->id]));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $article = Article::findOrFail($id);
        return view('articles.show', compact('article'));
//        return $article;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
//   * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *      * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $article = Article::find($id);
        $categories = ArticleCategory::all();

        return view('articles.edit', compact('article'), compact('categories'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


        $article = Article::find($id);

//        $article->save();
////        print_r($article);
        $validated = $request->validate([

            'title' => 'required|string',
            'content'  => 'string',
            'category' => 'required'
        ]);

//        // Create slug from title
//        $validated['slug'] = Str::slug($validated['title'], '-');
//
//        // Update Post with validated data
//        $article->update($validated);
//        $article->title = $validated['title'];

        if($request->publish==1){
            $article->status = $request->publish;
            $article->save();
        }

        $article->update(
            ['title' => $validated['title'],
                'content' => $validated['content'],
                'category' => $validated['category']
            ]
        );

//
//        // Redirect the user to the created post woth an updated notification
        $request->session()->flash('status-green', '"' . $article->title . '" has been updated.' );
        return redirect()->route('articles.show', ['article' => $article->id]);



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        //
        $article = Article::find($id);
        $article->delete();
        $request->session()->flash('status-red', '"' . $article->title . '" has been removed.' );
        return redirect(route('articles.index'));

    }

    public function changeStatus(Request $request, $id)
    {
        $article = Article::find($id);
        if($article->status == 1)
        {
            $article->status = 0;
            $request->session()->flash('status-yellow', '"' . $article->title . '" has been unpublished. It is now saved to draft.' );
        }
        else{
            $article->status = 1;
            $request->session()->flash('status-green', '"' . $article->title . '" has been published' );

        }
        $article->save();


        return redirect(route('articles.index'));

    }
    public function upload(Request $request)
    {
        if($request->hasFile('upload')) {
            //get filename with extension
            $filenamewithextension = $request->file('upload')->getClientOriginalName();

            //get filename without extension
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);

            //get file extension
            $extension = $request->file('upload')->getClientOriginalExtension();

            //filename to store
            $filenametostore = $filename.'_'.time().'.'.$extension;

            //Upload File
            $request->file('upload')->storeAs('public/uploads', $filenametostore);

            $CKEditorFuncNum = $request->input('CKEditorFuncNum');
            $url = asset('storage/uploads/'.$filenametostore);
            $msg = 'Image successfully uploaded';
            $re = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";

            // Render HTML output
            @header('Content-type: text/html; charset=utf-8');
            echo $re;
        }
    }
}
