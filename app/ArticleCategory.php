<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArticleCategory extends Model
{
    //

    protected $fillable =
        [

            'title',
            'slug',
            'description',
            'created_at',
            'updated_at',
            'upper_level',

        ];

    public function articles()
    {
        return $this->hasMany(Article::class, 'category_id');
    }
    public function parent(){

        return $this->belongsTo(ArticleCategory::class, 'upper_level');

    }

    public function subcategory(){

        return $this->hasMany(ArticleCategory::class, 'upper_level');

    }

}
