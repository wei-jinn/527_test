<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    //

    protected $fillable =
        [

            'title',
            'content',
            'status',
            'user_id',
            'created_at',
            'updated_at',
            'slug',
            'category_id'

        ];


    public function editors()
    {
        return $this->belongsToMany('App\ArticleUser', 'article_user', 'article_id', 'user_id');
    }

    public function author()
    {
        return $this->belongsTo(User::class);
    }

    public function category()
    {
        return $this->belongsTo(ArticleCategory::class);
    }



}
