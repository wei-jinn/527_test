<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ArticleCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        DB::table('article_categories')->insert([

            'title' => 'Entertainment',
            'slug' => 'entertainment',
            'description' => 'entertainment',
            'upper_level' => 0,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('article_categories')->insert([

            'title' => 'Local Entertainment',
            'slug' => 'local-entertainment',
            'description' => 'local-entertainment',
            'upper_level' => 1,
            'created_at' => now(),
            'updated_at' => now(),

        ]);

        DB::table('article_categories')->insert([

            'title' => 'Lifestyle',
            'slug' => 'lifestyle',
            'description' => 'lifestyle',
            'upper_level' => 0,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('article_categories')->insert([

            'title' => 'Health',
            'slug' => 'health',
            'description' => 'health',
            'upper_level' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('article_categories')->insert([

            'title' => 'Local Music',
            'slug' => 'local-music',
            'description' => 'local-music',
            'upper_level' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('article_categories')->insert([

            'title' => 'Eating Habits',
            'slug' => 'eating-habits',
            'description' => 'eating-habits',
            'upper_level' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('article_categories')->insert([

            'title' => 'Local Malay Music',
            'slug' => 'local-malay-music',
            'description' => 'local-malay-music',
            'upper_level' => 5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('article_categories')->insert([

            'title' => 'Local Chinese Music',
            'slug' => 'local-chinese-music',
            'description' => 'local-chinese-music',
            'upper_level' => 5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('article_categories')->insert([

            'title' => 'Modern Outfit Design',
            'slug' => 'modern-outfit-design',
            'description' => 'modern-outfit-design',
            'upper_level' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('article_categories')->insert([

            'title' => 'Local Chinese Music 1990',
            'slug' => 'local-chinese-music-1990',
            'description' => 'local-chinese-music-1990',
            'upper_level' => 8,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
