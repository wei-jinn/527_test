<?php

use App\ArticleCategory;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(ArticleSeeder::class);
        $this->call(ArticleCategorySeeder::class);
        $this->call(RolePermissionSeeder::class);
//        $this->call(UserSeeder::class);
    }
}
