<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $admin = Role::create(['name' => 'admin']);
        $site_manager = Role::create(['name' => 'site-manager']);
        $editor = Role::create(['name' => 'editor']);
        $customer = Role::create(['name' => 'customer']);

        $manage_articles = Permission::create(['name' => 'article-manage-articles']);
        $manage_categories = Permission::create(['name' => 'article-manage-categories']);
        $edit_as_author = Permission::create(['name' => 'article-edit-as-author']);

        $manage_users = Permission::create(['name' => 'user-manage-users']);
        $edit_as_user = Permission::create(['name' => 'user-edit-as-user']);
        $manage_roles = Permission::create(['name' => 'user-manage-roles']);

        $manage_translations = Permission::create(['name' => 'translation-manage-translations']);

        $manage_media = Permission::create(['name' => 'media-manage-media']);
        $manage_media_as_user = Permission::create(['name' => 'media-manage-media-as-user']);

        $admin_settings = Permission::create(['name' => 'admin-handle-settings']);

        $admin->syncPermissions(
            ['article-manage-articles',
             'article-manage-categories',
             'article-edit-as-author',
             'user-manage-users',
             'user-edit-as-user',
             'user-manage-roles',
             'translation-manage-translations',
             'media-manage-media',
             'media-manage-media-as-user',
             'admin-handle-settings'
            ]
        );

        $site_manager->syncPermissions(
            [
                'article-manage-articles',
                'article-manage-categories',
                'article-edit-as-author',
                'user-manage-users',
                'user-manage-roles',
                'user-edit-as-user',
                'translation-manage-translations',
                'media-manage-media',
                'media-manage-media-as-user',
            ]
        );

        $editor->syncPermissions(
            [
                'article-manage-categories',
                'article-edit-as-author',
                'user-edit-as-user',
                'media-manage-media-as-user',
            ]
        );

        $customer->syncPermissions(
            [

                'user-edit-as-user',
            ]
        );

    }
}
