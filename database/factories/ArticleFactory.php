<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Article;
use App\Model;
use App\User;
use Faker\Generator as Faker;

$factory->define(Article::class, function (Faker $faker) {

    $title = $faker->unique()->name;
    $lower_title = Str::lower($title);
    $slug = str_replace(" ","-", $lower_title);
    $slug = str_replace(".", "x", $slug);

    return [
        //
        'title' => $title,
        'content' => $faker->paragraph(),
        'status' => 1,
        'author_id' => factory(App\User::class), // password
        'created_at'=>now(),
        'updated_at'=>now(),
        'slug'=> $slug,
        'category_id'=>$faker->numberBetween($min=1, $max=4)
        ];
});
