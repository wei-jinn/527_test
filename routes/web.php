<?php

use App\User;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/testing', function(){

    return "This is a testing route";


});
Route::post('/roles/new-role', 'RoleController@storeRole')->name('roles.store.role');
Route::resource('roles', 'RoleController');
Route::get('/exists/{file}' , function($file){

    if(Storage::disk('public')->exists('Basic/branch')){

        return "File exists";
    }
    else{
        return "No such file";
    }

});



Route::resource('participants', 'ParticipantController');

Route::get('/upload', 'UserController@uploadPhoto')->name('upload.image');
Route::post('/uploadResult', 'UserController@upload')->name('upload.result');


Route::get('locale/{locale}', function($locale){
            Session::put('locale', $locale);
            return redirect()->back();
})->name('locale');


Route::get('/home', 'HomeController@index')->name('home');



    Route::get('/', function () {
        return view('welcome');
    })->name('welcome');

Route::resource('users', 'UserController');


Route::group(['middleware' => ['auth']], function () {


    //

    Route::group(['middleware' => ['permission:user-manage-roles']], function () {
        //

//        Route::post('/roles/new-role', 'RoleController@storeRole')->name('roles.store.role');
//        Route::resource('roles', 'RoleController');

    });

    Route::group(['middleware' => ['permission:user-manage-users']], function () {


        //        Route::resource('users', 'UserController');

    });

    Route::group(['middleware' => ['permission:article-manage-articles']], function () {
        //
        Route::post('articles/image_upload', 'ArticleController@upload')->name('upload');
        Route::get('articles/translate', 'ArticleController@translate')->name('translate');
        Route::get('articles/filter', 'ArticleController@filter')->name('articles.filter');
        Route::get('articles/change-status/{article}', 'ArticleController@changeStatus')->name('articles.changestatus');  //Always define the route for customised function of a resource controller before defining the resource controller.
        Route::resource('articles', 'ArticleController');
    });

    Route::group(['middleware' => ['permission:article-manage-categories']], function () {
        //
        Route::get('article-category/subcategories/{category}', 'ArticleCategoryController@listSubcategories')->name('article-category.subcategories');  //Always define the route for customised function of a resource controller before defining the resource controller.
        Route::resource('article-category', 'ArticleCategoryController');
    });

    Route::group(['middleware' => ['permission:article-manage-categories']], function () {
        //
        Route::get('article-category/subcategories/{category}', 'ArticleCategoryController@listSubcategories')->name('article-category.subcategories');  //Always define the route for customised function of a resource controller before defining the resource controller.
        Route::resource('article-category', 'ArticleCategoryController');
    });

    Route::group(['middleware' => ['permission:translation-manage-translations']], function () {

        // Route to translation manager interface : /translations
        // The middleware to control translation manager is written in app/config/translation-manager.php
        // Refer github  https://github.com/barryvdh/laravel-translation-manager

    });
    Route::group(['middleware' => ['permission:media-manage-media']], function () {
//        ctf0\MediaManager\MediaRoutes::routes();
    });
//    ctf0\MediaManager\MediaRoutes::routes();


});

ctf0\MediaManager\MediaRoutes::routes();
//Route::post('/roles/new-role', 'RoleController@storeRole')->name('roles.store.role');
//Route::resource('roles', 'RoleController');
////





// MediaManager

