/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * JQuery -> disable / enable
 */
$(document).ready(
    function(){
        $('input:file').change(
            function(){
                var ext = $('#profile_photo').val().split('.').pop().toLowerCase();
                if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
                    alert('invalid file type');
                    // if ($(this).val()) {
                }
                else{
                    // alert('file is chosen');
                    // $('#uploadPhoto').attr('disabled',false);
                    // $('#uploadPhoto').submit();
                    $('#profilePhoto').submit();
                }

            }
        );
    });


/**
 *  Vue
 */

window.Vue = require('vue');

Vue.component('welcomee', require('./components/Welcome.vue').default);

const app = new Vue({
    el: '#c1'
});

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

// Vue.component('example-component', require('./components/ExampleComponent.vue').default);
//
// /**
//  * Next, we will create a fresh Vue application instance and attach it to
//  * the page. Then, you may begin adding components to this application
//  * or customize the JavaScript scaffolding to fit your unique needs.
//  */

// require('../vendor/MediaManager/js/manager.js')

require('../assets/vendor/MediaManager/js/manager.js')




//
new Vue({
    el: '#media'
})

