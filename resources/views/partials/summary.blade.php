
@if($article->status ==1)
<div class="content">
    <a href="{{ route('articles.show', [$article->id]) }}">
        <h1 class="title">{{ $article->title }}</h1>
    </a>

    <div>
        <b>Posted: </b> {{ $article->created_at->diffForHumans() }}<br>
        <b>Last updated: </b> {{ $article->updated_at->diffForHumans() }}<br>
        <b>Category: </b>{{ $article->category->title }} <br>
    </div>


    <hr>
    <div>
{{--    {!! nl2br(e($article->content)) !!}--}}
    {!! $article->content !!}

    </div>
    <br>
    <hr>

    <div>
    <form method="post" action="{{ route('articles.destroy', [$article->id]) }}">
        @csrf @method('delete')
        <div class="field is-grouped">
            <span class="control">
                <a
                    href="{{ route('articles.edit', [$article->id])}}"
                    class="btn btn-outline-primary btn-space"
                >
                    Edit
                </a>
            </span>
            <span>     <a href="{{ route('articles.index')}}" class="btn btn-outline-primary btn-space">Return to Article List</a></span>
            <span class="control">
                <button type="submit" class="btn btn-outline-danger btn-space">
                    Delete
                </button>
            </span>

        </div>
    </form>
    </div>
</div>

@else
    <div><p>
            This article has been removed /has not been published yet.
        </p></div>

    <div>
        <a href="{{ route('articles.index')}}" class="btn btn-primary">Return to Article List</a>
    </div>

@endif
