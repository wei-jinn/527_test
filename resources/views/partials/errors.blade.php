@if ($errors->any())
    <div class="alert alert-warning" role="alert">
        <ul>
            @foreach ($errors->all() as $error)
                {{ $error }} <br>
            @endforeach
        </ul>
    </div>
@endif
