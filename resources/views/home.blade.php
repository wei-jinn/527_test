@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{__('g.Login')}}</div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        {{__('You are logged in!')}}


                    </div>

                </div>

                <div class="card-body">
                    <div>

                        @can('article-manage-articles')
                            <a href="{{ route('articles.index') }}"
                               class="btn btn-outline-dark btn-space">{{trans('g.Articles')}}</a>
                        @endcan
                        @can('user-manage-roles')
                            <a href="{{ route('roles.index') }}"
                               class=" btn btn-outline-dark btn-space">{{trans('g.Roles')}}</a>
                        @endcan
                        @can('user-manage-users')
                            {{--                    <a href="{{ route('users.index', app()->getLocale()) }}" class=" btn btn-outline-dark btn-space">User Management</a>--}}
                            <a href="{{ route('users.index') }}"
                               class=" btn btn-outline-dark btn-space">{{trans('g.Users')}}</a>
                        @endcan
                        @can('translation-manage-translations')
                                                <a href="{{ route('users.index', app()->getLocale()) }}" class=" btn btn-outline-dark btn-space">User Management</a>
                            <a href="/translations"
                                class=" btn btn-outline-dark btn-space">{{trans('g.Translation')}}</a>
                        @endcan
                        @can('translation-manage-translations')
                            {{--                    <a href="{{ route('users.index', app()->getLocale()) }}" class=" btn btn-outline-dark btn-space">User Management</a>--}}
                            <a href="/Media"
                                class=" btn btn-outline-dark btn-space">{{trans('g.Media')}}</a>
                        @endcan
                    </div>
                </div>

                @endsection
            </div>
        </div>
    </div>
