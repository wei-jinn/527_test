
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Laravel</title>
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 60px;
        }

        .links > a {
            /*color: #636b6f;*/
            /*padding: 0 25px;*/
            padding: 3 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }

        .standout{
            background-size: 200px;
        }
    </style>
</head>
<body>



{{--    <div id="c1">--}}
{{--        <welcomee :title="'This cool App'"></welcomee>--}}
{{--    </div>--}}
{{--    <script type="text/javascript" src="js/app.js"></script>--}}
<div class="flex-center position-ref full-height">


{{--    @if (Route::has('login'))--}}
{{--        <div class="top-right links">--}}

{{--        <div class="nav-link" aria-labelledby="navbarDropdown">--}}
{{--            <a class="dropdown-item" href="{{ route('logout') }}"--}}
{{--               onclick="event.preventDefault();--}}
{{--        document.getElementById('logout-form').submit();">--}}
{{--                {{ trans('g.Logout') }}--}}
{{--            </a>--}}

{{--            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">--}}
{{--                @csrf--}}
{{--            </form>--}}
{{--        </div>--}}

{{--            <a class="nav-link" href="{{route('locale', ['locale' => 'en'])}}">EN</a>--}}
{{--            <a class="nav-link" href="{{route('locale', ['locale' => 'cn'])}}">中文</a>--}}
{{--            <a class="nav-link" href="{{route('locale', ['locale' => 'my'])}}">B.Malaysia</a>--}}

{{--            @auth--}}
{{--                <a href="{{ route('home') }}">{{trans('g.Home')}}</a>--}}
{{--            @else--}}
{{--                <a href="{{ route('login') }}">{{trans('g.Login')}}</a>--}}
{{--                @if (Route::has('register'))--}}
{{--                    <a href="{{ route('register') }}">{{trans('g.Register')}}</a>--}}
{{--                @endif--}}
{{--            @endauth--}}
{{--        </div>--}}
{{--    @endif--}}

    <div class="content">
        @if (session('status-green'))
            <div class="alert alert-success standout">
                {{ session('status-green') }}
            </div>

        @elseif(session('status-red'))
            <div class="alert alert-danger standout">
                {{ session('status-red') }}
            </div>

        @elseif(session('status-yellow'))
            <div class="alert alert-warning standout">
                {{ session('status-yellow') }}
            </div>
        @endif
        <div class="title m-b-md">
          PFSKL

                <img src="{{asset('storage/pfslogo.jpg')}}" style="width:700px;height:auto;">


        </div>

        <div class="links">
            <a href="{{ route('participants.index')}}" class="btn btn-primary btn-space float-right">Participant List</a>
            <a href="{{ route('participants.draw')}}" class="btn btn-success btn-space float-right">Lucky Draw</a>
            <a href="{{ route('participants.luckydraw')}}" class="btn btn-dark btn-space float-right">Refresh</a>

{{--            <a href="{{route('articles.index')}}">{{trans('g.Articles')}}</a>--}}
{{--            <a href="{{route('users.index')}}">{{trans('g.Users')}}</a>--}}
{{--            <a href="{{route('roles.index')}}">{{trans('g.Roles')}}</a>--}}
{{--            <a href="/translations">{{trans('g.Translation')}}</a>--}}
{{--            <a href="/media">{{trans('g.Media')}}</a>--}}
            {{--                    <a href="/media">{{trans('g.Media')}}</a>--}}
            {{--                    <a href="/media">{{trans('g.Media')}}</a>--}}

        </div>
    </div>
</div>
</body>
</html>
