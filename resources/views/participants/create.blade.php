@extends('layouts.app2')

@section('content')

    @if (session('status-green'))
        <div class="alert alert-success">
            {{ session('status-green') }}
        </div>

    @elseif(session('status-red'))
        <div class="alert alert-danger">
            {{ session('status-red') }}
        </div>

    @elseif(session('status-yellow'))
        <div class="alert alert-warning">
            {{ session('status-yellow') }}
        </div>
    @endif

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Create a new participant') }}

                        <a href="{{ route('participants.index')}}" class="btn btn-outline-primary btn-space float-right">Participant List</a>

                    </div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('participants.store') }}" autocomplete="off">
                            @csrf

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="" required>

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>

                                    </span>
                                    @enderror
                                </div>
                            </div>


                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-outline-success">
                                        {{ __('Create') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
