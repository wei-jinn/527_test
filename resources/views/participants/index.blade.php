@extends('layouts.app2')


@section('content')
    @if (session('status-green'))
        <div class="alert alert-success">
            {{ session('status-green') }}
        </div>

    @elseif(session('status-red'))
        <div class="alert alert-danger">
            {{ session('status-red') }}
        </div>

    @elseif(session('status-yellow'))
        <div class="alert alert-warning">
            {{ session('status-yellow') }}
        </div>
    @endif
    <div class="container">
        {{--        <div class="row justify-content-center">--}}

        <div class="">
            <div class="card">
                <div class="card-header">
                    List of Participants
                    <a href="{{ route('participants.create') }}" class="float-right btn btn-success btn-space" >Create new participants</a>
                    <a href="{{ route('participants.luckydraw') }}" class="float-right btn btn-dark btn-space" >Lucky draw</a>
                </div>

                <div>
                    <table class="article-category-list">
                        <tr>

                            <th>Username</th>
                            <th>Prize</th>



                        </tr>
                        @foreach ($participants as $participant)
                            <tr>

                                {{--                                <th><img src="{{ $user->photo? $user->photo : asset('storage/Basic/profile-placeholder.png')  }}" class="img-thumbnail rounded" width="50" height="50"></th>--}}
{{--                                <th><img src="{{$user->photo? asset('storage/uploads/user/profile/'. $user->photo) : asset('storage/Basic/profile-placeholder.png') }}" alt="" width="50" height="50" ></th>--}}
{{--                                <td>{{$user->id}}</td>--}}
                                <td>{{$participant->name}}</td>
{{--                                <td>{{$user->email}}</td>--}}

{{--                                <td>{{empty($user->getRoleNames()[0])? '-' : $user->getRoleNames()[0]}}</td>--}}
                                <td>{{$participant->prize}}</td>

{{--                                <td><a href="#" class="btn btn-outline-primary btn-space">Edit</a></td>--}}
{{--                                <td><a href="{{ route('users.edit', [ 'participant' => $participant->id])}}" class="btn btn-outline-primary btn-space">Edit</a></td>--}}

                                <td>
                                    <form method="post" action="{{ route('participants.destroy', [ $participant->id]) }}">
                                        @csrf @method('delete')

                                        <div class="control">
                                            <button type="submit" class="btn btn-outline-danger btn-space" onclick="return confirm('Are you sure you want to delete this participant?')">
                                                Delete
                                            </button>
                                        </div>

                                    </form>

                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>

    </div>

@endsection
