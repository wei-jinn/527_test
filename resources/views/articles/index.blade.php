@extends('layouts.app')

@section('content')

    @if (session('status-green'))
        <div class="alert alert-success">
            {{ session('status-green') }}
        </div>

    @elseif(session('status-red'))
        <div class="alert alert-danger">
            {{ session('status-red') }}
        </div>

    @elseif(session('status-yellow'))
        <div class="alert alert-warning">
            {{ session('status-yellow') }}
        </div>
    @endif
    {{--    <br><br>--}}
    <div class="container">
        {{--        <div class="row justify-content-center">--}}

        <div class="">
            <div class="card">

                <div class="card-header">
                    Articles
                    <a href="{{ route('articles.create' )}}" class="float-right btn btn-success btn-space">Create an
                        article</a>
                    <a href="{{ route('article-category.index') }}" class="float-right btn btn-outline-dark btn-space">Manage
                        Categories</a>
                    <a href="{{ route('home') }}" class="float-right btn btn-outline-dark btn-space">Home</a>
                </div>

                <div class="card-header">

                    <form method="get" action="{{ route('articles.filter')}}">
                        <div class="form-group">


                            {{--                                    <label for="filter-start-date">From</label>--}}
                            <input type="date" name="filter-start-date" value="{{$selected_start_date}}">

                            <label for="filter-start-date">To</label>
                            <input type="date" name="filter-end-date" value="{{$selected_end_date}}">

                            <select name="filter-category" class="custom-select custom-select-md col-sm-2">

                                @if($selected_category)
                                    <option value="{{$selected_category->id}}" selected
                                            hidden>{{$selected_category->title}}</option>
                                @endif
                                <option value="0">All Categories</option>
                                @foreach($categories as $category)

                                    <option value="{{$category->id}}">{{$category->title}}</option>

                                @endforeach
                            </select>

                            <select name="filter-author" class="custom-select custom-select-md col-sm-2">
                                @if($selected_author)
                                    <option value="{{$selected_author->id}}" selected
                                            hidden>{{$selected_author->name}}</option>
                                @endif
                                <option value="0">All Authors</option>
                                @foreach($authors as $author)
                                    <option value="{{$author->id}}">{{$author->name}}</option>
                                @endforeach
                            </select>
                            @if($selected_keywords)
                                <input type="text" name="filter-keywords" placeholder="" value="{{$selected_keywords}}">
                            @else
                                <input type="text" name="filter-keywords" placeholder="Search keywords...">
                            @endif

                            <button type="submit" class="btn btn-space btn-outline-dark">Filter</button>
                            <a href="{{ route('articles.index') }}" class=" btn btn-outline-dark btn-space">Clear
                                Filter</a>
                        </div>


                    </form>


                    <div>
                        <div>


                        </div>
                        <table class="article-list">
                            <tr>
                                <th>ID</th>
                                <th>Title</th>
                                <th>Category</th>
                                <th>Author</th>
                                <th>Status</th>
                                <th>Created at</th>

                            </tr>
                            @foreach ($articles as $article)
                                <tr>
                                    <td>{{$article->id}}</td>
                                    <td>{{$article->title}}</td>
                                    {{--                                <td>{!! substr($article->content, 0, 30) . "..." !!} </td>--}}
                                    <td>{{$article->category->title}}</td>
                                    {{--                                <td>{{$article->category_id}}</td>--}}
                                    <td>{{$article->author->name}}</td>
                                    {{--                                <td>{{$article->author_id}}</td>--}}
                                    <td>{{$article->status==1? "Published" : "Draft"}}</td>
                                    <td>{{$article->created_at?  $article->created_at : "N/A"}}</td>
                                    {{--                                <td>{{$article->updated_at?  $article->updated_at->diffForHumans() : "N/A"}}</td>--}}
                                    <td><a href="{{ route('articles.show', [ 'article' => $article->id  ])}}"
                                           class="btn btn-outline-primary btn-space">Read</a></td>
                                    <td><a href="{{ route('articles.edit', [  'article' => $article->id])}}"
                                           class="btn btn-outline-primary btn-space">Edit</a></td>
                                    <td><a href="{{ route('articles.changestatus', [ 'article' => $article->id])}}"
                                           class="btn btn-outline-primary btn-space">{{$article->status==1? "Unpublish": "Publish"}}</a>
                                    </td>
                                    <td>
                                        <form method="post" action="{{ route('articles.destroy', [ 'article' => $article->id]) }}">
                                            @csrf @method('delete')

                                            <div class="control">
                                                <button type="submit" class="btn btn-outline-danger btn-space"
                                                        onclick="return confirm('Are you sure you want to delete this article?')">
                                                    Delete
                                                </button>
                                            </div>

                                        </form>

                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>

        </div>
@endsection
