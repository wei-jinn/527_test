<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Document editor</title>


</head>
<body>
<h1>Document editor</h1>

<!-- The toolbar will be rendered in this container. -->
<div id="toolbar-container"></div>

<div class="field">
    <label class="label">請在以下空白編輯文章</label>
    <textarea class="form-control" id="summary-ckeditor" name="content"></textarea>

    <!-- The toolbar will be rendered in this container. -->
    <div id="toolbar-container"></div>


</div>
<script src="{{ asset('ckeditor4/ckeditor.js') }}"></script>


<script>

    CKEDITOR.replace('summary-ckeditor', {
        filebrowserUploadUrl: "{{route('upload', ['_token' => csrf_token() ])}}",
        filebrowserUploadMethod: 'form'
    });

    // ----------------------CKEDITOR 5.0---------------------------
    // DecoupledEditor
    //     .create( document.querySelector( '#editor' ),
    //
    //
    //     )
    //     .then( editor => {
    //         const toolbarContainer = document.querySelector( '#toolbar-container' );
    //
    //         toolbarContainer.appendChild( editor.ui.view.toolbar.element );
    //     } )
    //
    //     .catch( error => {
    //         console.error( error );
    //     } );
    //
    // DecoupledEditor.builtinPlugins.map( plugin => plugin.pluginName );

</script>
</body>
</html>
