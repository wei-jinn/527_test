@extends('layouts.app')

@section('content')

    <h1 class="title">Create a new category</h1>

    <form method="post" action="{{ route('article-category.store') }}">

        @csrf

        @include('partials.errors')
        <div class="field">
            <label class="label">Category Title</label>
            <div class="control">
                <input type="text" name="title" value="{{ old('title') }}" class="input" placeholder="Title" maxlength="100" required />
            </div>
            <label class="label">Category Slug</label>
            <div class="control">
                <input type="text" name="slug" value="{{ old('slug') }}" class="input" placeholder="slug" maxlength="100" required />
            </div>
        </div>

        <div class="field">
            <label class="label">Category Description</label>

            <textarea class="form-control" id="" name="description"></textarea>
            <!-- The toolbar will be rendered in this container. -->
            <div id="toolbar-container"></div>
        </div>

        <div class="field">
            <label class="label">Parent Category</label>
            <div class="control">
                <div class="select">
                    <select name="parent-category" >
                        <option value="" disabled hidden selected>Select category</option>
                        <option value="0">No Parent Category</option>
                        @foreach($categories as $category)
                            <option value="{{$category->id}}" >{{$category->title}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>

{{--        <div class="field">--}}
{{--            <label class="label">Category</label>--}}
{{--            <div class="control">--}}
{{--                <div class="select">--}}
{{--                    <select name="category" required>--}}
{{--                        <option value="" disabled selected>Select category</option>--}}
{{--                        <option value="Education" {{ old('category') === 'Education' ? 'selected' : null }}>Education</option>--}}
{{--                        <option value="Business" {{ old('category') === 'Business' ? 'selected' : null }}>Business</option>--}}
{{--                        <option value="Entertainment" {{ old('category') === 'Entertainment' ? 'selected' : null }}>Entertainment</option>--}}
{{--                        <option value="Sports" {{ old('category') === 'Sports' ? 'selected' : null }}>Sports</option>--}}
{{--                        <option value="Relationship" {{ old('category') === 'Relationship' ? 'selected' : null }}>Relationship</option>--}}
{{--                    </select>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}

        <br>
        <div class="field">
            <div class="control">
                <button type="submit" class="button is-link is-outlined">Create</button>
                <a href="{{ route('article-category.index')}}" class="btn btn-outline-primary btn-space">Return to Category List</a>
            </div>
        </div>

    </form>

@endsection


