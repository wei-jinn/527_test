@foreach($subcategories as $subcategory)



    <ul >
        <li>
            <a href="{{ route('article-category.edit', [$subcategory->id])}}" class="btn btn-light">{{$subcategory->title}}</a>
        </li>
        @if(count($subcategory->subcategory))
            @include('articles.categories.category-treeview-secondary',['subcategories' => $subcategory->subcategory])

        @endif
    </ul>

@endforeach
