@extends('layouts.app')


@section('content')
    @if (session('status-green'))
        <div class="alert alert-success">
            {{ session('status-green') }}
        </div>

    @elseif(session('status-red'))
        <div class="alert alert-danger">
            {{ session('status-red') }}
        </div>

    @elseif(session('status-yellow'))
        <div class="alert alert-warning">
            {{ session('status-yellow') }}
        </div>
    @endif
    <div class="container">
        {{--        <div class="row justify-content-center">--}}

        <div class="">
            <div class="card">
                <div class="card-header">
                    Article Categories(Tree mode)
                    <a href="{{ route('article-category.create') }}" class="float-right btn btn-success btn-space">Create a category</a>
                    <a href="{{ route('article-category.index') }}" class="float-right btn btn-outline-dark btn-space">Article Category List</a>
                </div>

                <div>
                        @foreach ($parents as $category)
                                    <a href="{{ route('article-category.edit', [$category->id])}}" class="btn btn-light">{{$category->title}}</a>
                            @if(count($category->subcategory))
                                @include('articles.categories.category-treeview-secondary',['subcategories' => $category->subcategory])
                            @endif

                        @endforeach
                </div>
            </div>
        </div>

    </div>

@endsection


