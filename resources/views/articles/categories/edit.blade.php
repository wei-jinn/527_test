@extends('layouts.app')

@section('content')

    <h1 class="title">Edit category</h1>

    <form method="post" action="{{ route('article-category.update', [$current_category->id]) }}">

        @csrf
        @method('patch')
        @include('partials.errors')

        @include('partials.errors')
        <div class="field">
            <label class="label">Category Title</label>
            <div class="control">
                <input type="text" name="title" value="{{ $current_category->title}}" class="input" placeholder="Title" minlength="5" maxlength="100" required />
            </div>
            <label class="label">Category Slug</label>
            <div class="control">
                <input type="text" name="slug" value="{{ $current_category->slug}}" class="input" placeholder="slug" maxlength="100" required />
            </div>
        </div>

        {{--        <div class="field">--}}
        {{--            <label class="label">Category Title</label>--}}
        {{--            <div class="control">--}}
        {{--                <input type="textarea" name="title" value="{{ old('description') }}" class="input" placeholder="Description" required />--}}
        {{--            </div>--}}
        {{--        </div>--}}


        <div class="field">
            <label class="label">Category Description</label>

            <textarea class="form-control" id="" name="description">{{$current_category->description}}</textarea>
            <!-- The toolbar will be rendered in this container. -->
            <div id="toolbar-container"></div>
        </div>

        <hr>
        <div class="field">
            <label class="label">Parent Category</label>
            <div class="control">
                <div class="select">
                    <select name="parent-category">
                        <option value="0">No Parent Category</option>
                        @foreach($categories as $category)
                            @if($current_category->upper_level !=0)
                                <option value="{{$current_category->parent->id}}" selected hidden>{{$current_category->parent->title}}</option>
                            @endif
                                <option value="{{$category->id}}" >{{$category->title}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

        </div>



        <br>
        <div class="field">
            <div class="control">
                <button type="submit" class="btn btn-outline-primary btn-space">Edit</button>
                <a href="{{route('article-category.index')}}" class="btn btn-outline-primary btn-space">Return to Article Categories</a>

            </div>
        </div>

    </form>


@endsection


