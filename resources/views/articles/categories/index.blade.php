@extends('layouts.app')


@section('content')
    @if (session('status-green'))
        <div class="alert alert-success">
            {{ session('status-green') }}
        </div>

    @elseif(session('status-red'))
        <div class="alert alert-danger">
            {{ session('status-red') }}
        </div>

    @elseif(session('status-yellow'))
        <div class="alert alert-warning">
            {{ session('status-yellow') }}
        </div>
    @endif
<div class="container">
    {{--        <div class="row justify-content-center">--}}

    <div class="">
        <div class="card">
            <div class="card-header">
                Article Categories
                <a href="{{ route('article-category.create') }}" class="float-right btn btn-success btn-space" >Create a category</a>
                <a href="{{ route('articles.index') }}" class="float-right btn btn-outline-dark btn-space">Article List</a>
                <a href="{{ route('article-category.subcategories', 0) }}" class="float-right btn btn-outline-dark btn-space">View in Tree Mode</a>
            </div>

            <div>
                <table class="article-category-list">
                    <tr>
                        <th>ID</th>
                        <th>Title</th>
                        <th>Slug</th>
                        <th>Description</th>
                        <th>Parent Categories</th>

                    </tr>
                    @foreach ($categories as $category)
                        <tr>
                            <td>{{$category->id}}</td>
{{--                            <td>{{}}</td>--}}
                            <td>{{$category->title}}</td>
                            <td>{{$category->slug}}</td>
                            <td>{{$category->description}}</td>
                            <td>{{$category->parent? $category->parent->title : "-" }}</td>
                            @if(count($category->subcategory)>0)
                            <td><a href="{{ route('article-category.subcategories', [$category->id])}}" class="btn btn-outline-primary btn-space">Subcategories</a></td>
{{--                            <td><a href="{{ route('article-category.subcategories', [$category->id])}}" class="btn btn-outline-primary btn-space">{{count($category->subcategory)}}</a></td>--}}
                            @else
                                <td><a href="{{ route('article-category.subcategories', [$category->id])}}" class="btn btn-outline-primary disabled btn-space">Subcategories</a></td>
                            @endif
                                <td><a href="{{ route('article-category.edit', [$category->id])}}" class="btn btn-outline-primary btn-space">Edit</a></td>

                                <td>
                                <form method="post" action="{{ route('article-category.destroy', [$category->id]) }}">
                                    @csrf @method('delete')

                                    <div class="control">
                                        <button type="submit" class="btn btn-outline-danger btn-space" onclick="return confirm('Are you sure you want to delete this category?')">
                                            Delete
                                        </button>
                                    </div>

                                </form>

                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>

</div>

    @endsection
