@extends('layouts.app')

@section('content')

    @if (session('status-green'))
        <div class="alert alert-success">
            {{ session('status-green') }}
        </div>

    @elseif(session('status-red'))
        <div class="alert alert-danger">
            {{ session('status-red') }}
        </div>

    @elseif(session('status-yellow'))
        <div class="alert alert-warning">
            {{ session('status-yellow') }}
        </div>
    @endif
    {{--    <br><br>--}}
    <div class="container">
        {{--        <div class="row justify-content-center">--}}

        <div class="">
            <div class="card">

                <div class="card-header">
                    Articles
                    <a href="{{ route('articles.create' )}}" class="float-right btn btn-success btn-space">Create an
                        article</a>
                    <a href="{{ route('article-category.index') }}" class="float-right btn btn-outline-dark btn-space">Manage
                        Categories</a>
                    <a href="{{ route('home') }}" class="float-right btn btn-outline-dark btn-space">Home</a>
                </div>

                <div class="card-header">

                            {!!$result !!}

                    <div>
                        <div class="card-body">



                        </div>

                    </div>
                </div>
            </div>

        </div>
@endsection
