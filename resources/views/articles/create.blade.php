@extends('layouts.app')

@section('content')

    <h1 class="title">Create a new article</h1>

    <form method="post" action="{{ route('articles.store') }}">

        @csrf

        @include('partials.errors')
        <div class="field">
            <label class="label">Title</label>
            <div class="control">
                <input type="text" name="title" value="{{ old('title') }}" class="input" placeholder="Title"
                        required/>
            </div>
        </div>


        <div class="field">
            <label class="label">Content</label>

            <textarea class="form-control" id="summary-ckeditor" name="content"></textarea>

            <div id="toolbar-container"></div>


        </div>

        <div class="field">
            <label class="label">Category</label>
            <div class="control">
                <div class="select">
                    <select name="category" required>
                        <option value="" disabled selected>Select category</option>
                        @foreach($categories as $category)
                            <option value="{{$category->id}}">{{$category->title}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>

        <br>
        <div class="field">
            <div class="control">
                <button type="submit" class="btn btn-outline-primary">Publish</button>
                <a href="{{ route('articles.index')}}" class="btn btn-outline-primary btn-space">Return to Article List</a>

            </div>
        </div>

    </form>

@endsection


