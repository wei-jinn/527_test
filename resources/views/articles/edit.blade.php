@section('title', 'Edit Article')
@section('action', route('articles.create'))
@extends('layouts.app')

@section('content')

    <h1 class="title">Edit: {{ $article->title }}</h1>

    <form method="post" action="{{ route('articles.update', [$article->id])}}">

        @csrf
        @method('patch')
        @include('partials.errors')

        <div class="field">
            <label class="label">Title</label>
            <div class="control">
                <input type="text" name="title" value="{{ $article->title }}" class="input" placeholder="Title"
                       minlength="5" maxlength="100" required/>
            </div>
        </div>

        <div class="field">
            <label class="label">Content</label>
            <div class="control">
                <textarea class="form-control" id="summary-ckeditor" name="content">{{$article->content}}</textarea>

            </div>
        </div>

        <div class="field">
            <label class="label">Category</label>
            <div class="control">
                <div class="select">
                    <select name="category" required>
                        @foreach($categories as $category)
                            <option value="{{$category->id}}">{{$category->title}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>

        <br>

        @if($article->status!=1)
            <input type="checkbox" id="publish" name="publish" value="1">
            <label for="publish">Publish this article once it is updated.</label>
        @endif
        <br>

        <div class="field">
            <span>
                <button type="submit" class="btn btn-outline-primary">Update</button></span>
            <span>
                <a href="{{ route('articles.index')}}" class="btn btn-outline-primary">Return to Article List</a>
            </span>
        </div>

    </form>



@endsection
