<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>

{{--    <div id="c1">--}}
{{--        <welcomee :title="'This cool App'"></welcomee>--}}
{{--    </div>--}}
{{--    <script type="text/javascript" src="js/app.js"></script>--}}
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">

                        <a class="nav-link" href="{{route('locale', ['locale' => 'en'])}}">EN</a>
                        <a class="nav-link" href="{{route('locale', ['locale' => 'cn'])}}">中文</a>
                        <a class="nav-link" href="{{route('locale', ['locale' => 'my'])}}">B.Malaysia</a>

                    @auth
{{--                        <a href="{{ route('home') }}">{{trans('g.Home')}}</a>--}}
                        <a href="{{ route('home') }}">{{trans('Lucky Draw')}}</a>
                    @else
                        <a href="{{ route('login') }}">{{trans('g.Login')}}</a>
                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">{{trans('g.Register')}}</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                   {{trans('g.The Rocket Project')}}
                </div>

                <div class="links">

                    <a href="{{route('articles.index')}}">{{trans('g.Articles')}}</a>
                    <a href="{{route('users.index')}}">{{trans('g.Users')}}</a>
                    <a href="{{route('roles.index')}}">{{trans('g.Roles')}}</a>
                    <a href="/translations">{{trans('g.Translation')}}</a>
                    <a href="/media">{{trans('g.Media')}}</a>
                    <img src="{{ asset('storage/Basic/profile-placeholder.png') }}" alt=""  class="img-thumbnail rounded" >

                    {{--                    <a href="/media">{{trans('g.Media')}}</a>--}}
{{--                    <a href="/media">{{trans('g.Media')}}</a>--}}

                </div>
            </div>
        </div>
    </body>
</html>
