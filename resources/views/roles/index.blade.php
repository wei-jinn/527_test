@extends('layouts.app')

@section('content')

    @if (session('status-green'))
        <div class="alert alert-success">
            {{ session('status-green') }}
        </div>

    @elseif(session('status-red'))
        <div class="alert alert-danger">
            {{ session('status-red') }}
        </div>

    @elseif(session('status-yellow'))
        <div class="alert alert-warning">
            {{ session('status-yellow') }}
        </div>
    @endif

    <div class="container">


        <div class="">
            <div class="card">

                <div class="card-header">
                    Role Management
                    <div class="d-inline-block float-right ">
                        <div class="d-inline-block">
                            <form class="form-inline" method="get" action="{{ route('roles.index')}}">
                                <select name="role-option" class="custom-select custom-select-md row btn-space">

                                    <option value="" hidden disabled selected>Select Role</option>
                                    @foreach($roles as $role)
                                        @if($selected_role)
                                            <option value="{{$selected_role->id}}" selected disabled
                                                    hidden>{{$selected_role->name}}</option>
                                        @endif
                                        <option value="{{$role->id}}">{{$role->name}}</option>
                                    @endforeach
                                </select>
                                <button type="submit" class="btn btn-space btn-outline-dark">Select</button>

                            </form>
                        </div>
                        <div class="d-inline-block">
                            @if($selected_role)
                                <form class="form-inline" method="post" action="{{ route('roles.destroy', [$selected_role->id]) }}">
                                    @csrf @method('delete')
                                    <button type="submit" class="btn btn-outline-danger btn-space" onclick="return confirm('Are you sure you want to delete this role?')">
                                        Delete Role: {{ str_replace("-", " ", ucfirst($selected_role->name)) }}
                                    </button>
                                </form>git
                            @endif
                        </div>
                        <div class="d-inline-block"><a href="{{ route('roles.create') }}" class=" btn btn-success btn-space" >Create new role</a></div>
                        <div class="d-inline-block"> <a href="{{ route('home') }}" class=" btn btn-outline-dark btn-space">Home</a></div>




                    </div>




                </div>


                <div class="row">
                    <div class="col-sm-2"><strong>Types</strong></div>
                    <div class="col-sm-10"><strong>Permissions</strong></div>
                </div>

                <hr>
                <div>
                <form method="post" action="{{ route('roles.store')}}">
                    @csrf
                    @if($selected_role)
                        <input class="d-lg-none" name="selected-role" value="{{$selected_role->id}}">
                    @endif
                    @foreach($permission_types as $key => $value)


                        <div class="row">
                            <div class="col-sm-2">{{$permission_types[$key]}}</div>
                            <div class="d-flex flex-column">


                                @foreach($permissions as $permission)

                                    @if(explode('-', $permission->name)[0] == $permission_types[$key])

                                        @if($selected_role && $selected_role->hasPermissionTo($permission->name))

                                            <div class="">
                                                <input class="" type="checkbox" name="permissions[]"
                                                       value="{{$permission->id}}" checked>
                                                <label class="form-check-label" for="defaultCheck1">
                                                    {{$permission->name}}
                                                </label>
                                            </div>
                                        @else
                                            <div class="">
                                                <input class="" type="checkbox" name="permissions[]"
                                                       value="{{$permission->id}}">
                                                <label class="form-check-label" for="defaultCheck1">
                                                    {{$permission->name}}
                                                </label>
                                            </div>
                                        @endif

                                    @endif
                                @endforeach
                            </div>
                        </div>

                        <hr>
                    @endforeach


                    @if($selected_role)

                        <button type="submit" class="btn btn-space btn-outline-dark"
                                onclick="return confirm('Confirm assigning these permissions?')">Confirm Assign
                        </button>

                    @else

                        <button type="submit" class="d-inline btn btn-space btn-outline-dark" disabled>Confirm Assign
                        </button>

                    @endif
                    </div>


                </form>


            </div>

            </div>
            <div>
            </div>
        </div>
{{--    </div>--}}

{{--    </div>--}}
@endsection
