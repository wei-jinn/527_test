@extends('layouts.app')

@section('content')

    <h1 class="title">Create a new role</h1>

    <form method="post" action="{{ route('roles.store.role') }}">

        @csrf

        @include('partials.errors')

        <div class="field">
            <label class="label">Role name</label>
            <div class="control">
                <input type="text" name="role-name" value="{{ old('role-name') }}" class="input" placeholder="Name a new role" required />
            </div>

        </div>


        <br>
        <div class="field">
            <div class="control">
                <button type="submit" class="btn btn-outline-primary btn-space">Create</button>
                <a href="{{ route('roles.index')}}" class="btn btn-outline-primary btn-space">Return to Role Management</a>
            </div>
        </div>

    </form>

@endsection


