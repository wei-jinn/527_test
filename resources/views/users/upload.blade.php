@extends('layouts.app')

@section('content')
    @if (session('status-green'))
        <div class="alert alert-success">
            {{ session('status-green') }}
        </div>

    @elseif(session('status-red'))
        <div class="alert alert-danger">
            {{ session('status-red') }}
        </div>

    @elseif(session('status-yellow'))
        <div class="alert alert-warning">
            {{ session('status-yellow') }}
        </div>
    @endif
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Edit User Profile') }}</div>
                    <div class="card-body">


                        <form method="POST" action="{{ route('upload.result',[ 'user' => $user->id]) }}" enctype="multipart/form-data">
                            @csrf


                            @include('partials.errors')

                            <div class="d-flex justify-content-center " >
                                <img src="{{$user->photo? asset('storage/images/'. $user->photo) : asset('storage/Basic/profile-placeholder.png') }}" alt=""  class="img-thumbnail rounded" >
                            </div>

                            <br>

                            <div class="form-group row">
                                <label for="photo" class="col-md-4 col-form-label text-md-right">{{ __('Profile Picture') }}</label>

                                <div class="col-md-6">
                                    <input type="file" class="form-control-file" name="photo"  >

                                    @error('photo')
                                    <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>

                                                                </span>
                                    @enderror
                                </div>
                            </div>


                            <br>
                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Edit') }}
                                    </button>
                                    <a href="{{route('users.index')}}" class="btn btn-primary">
                                        {{ __('Return to User List') }}
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
