@extends('layouts.app')

@section('content')
    @if (session('status-green'))
        <div class="alert alert-success">
            {{ session('status-green') }}
        </div>

    @elseif(session('status-red'))
        <div class="alert alert-danger">
            {{ session('status-red') }}
        </div>

    @elseif(session('status-yellow'))
        <div class="alert alert-warning">
            {{ session('status-yellow') }}
        </div>
    @endif
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Edit User Profile') }}</div>
                    <div class="card-body">


                        <div class="d-flex justify-content-center " >
                            <form method="POST" id="profilePhoto" action="{{ route('upload.result',[ 'user' => $user->id]) }}" enctype="multipart/form-data" >
                                @csrf


                                @include('partials.errors')

                                <div class="d-flex justify-content-center " >
                                    <img src="{{$user->photo? asset('storage/uploads/user/profile/'. $user->photo) : asset('storage/Basic/profile-placeholder.png') }}" alt=""  class="img-thumbnail rounded" >
                                </div>

                                <br>

                                <div class="form-group row">

                                    <div class="col-md-6 offset-md-4">
                                        <input type="file" id="profile_photo" class="form-control-file" name="photo" accept="image/*" >

                                        @error('photo')
                                        <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>

                                                                </span>
                                        @enderror
                                    </div>
                                </div>



                            </form>
                        </div>



                        <form method="POST" action="{{ route('users.update',[ 'user' =>$user->id]) }}" enctype="multipart/form-data">
                            @csrf
                            @method('patch')
                            @include('partials.errors')



                            <br>




                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $user->name }}" required autofocus>

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{$user->email }}" required readonly>

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="birthdate" class="col-md-4 col-form-label text-md-right">{{ __('Birth Date') }}</label>

                                <div class="col-md-6">
                                    <input type="date" class="form-control"  name="birthdate" value="{{$user->birthdate}}">

                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="phone" class="col-md-4 col-form-label text-md-right">{{ __('Phone Number') }}</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="phone" value="{{$user->phone_number}}" >

                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="address" class="col-md-4 col-form-label text-md-right">{{ __('Address') }}</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="address" value="{{$user->address}}" >
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Role') }}</label>

                                <div class="col-md-6">
                                    <div class="d-inline-block">

                                        <select name="role" class="custom-select custom-select-md row btn-space">
                                            @if($user_role)
                                                <option hidden value="{{$user_role->id}}">{{$user_role->name}}</option>
                                            @else
                                                <option  hidden disabled selected>Select Role</option>
                                            @endif
                                            @foreach($roles as $role)
                                                <option value="{{$role->id}}">{{$role->name}}</option>
                                            @endforeach
                                            <option value="no-role">No Role</option>
                                        </select>

                                    </div>

                                </div>
                            </div>

                            <div class="bg-light text-dark">
                            <div class="d-flex justify-content-center font-italic" >
                                Leave the columns below blank if you are not going to change your password.
                            </div>
                            <br>

                            <div class="form-group row">
                                <label for="old-password" class="col-md-4 col-form-label text-md-right">{{ __('Old Password') }}</label>

                                <div class="col-md-6">
                                    <input id="old-password" type="password" class="form-control" name="old-password"  >
{{--                                    <input id="old-password" type="password" class="form-control @error('old-password') is-invalid @enderror" name="old-password"  >--}}

{{--                                    @error('old-password')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                    @enderror--}}
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('New Password') }}</label>

                                <div class="col-md-6">
{{--                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" >--}}
                                    <input id="password" type="password" class="form-control " name="password" >

{{--                                    @error('password')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                    @enderror--}}
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm New Password') }}</label>

                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" >
                                </div>
                            </div>

                                <br>
                            </div>

                            <br>
                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Save Changes') }}
                                    </button>
                                    <a href="{{route('users.index')}}" class="btn btn-primary">
                                        {{ __('Return to User List') }}
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
